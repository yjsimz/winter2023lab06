public class Board 
{
	private Die die1;
	private Die die2;
	private boolean[] tiles;
	
	public Board() 
	{
		die1 = new Die();
		die2 = new Die();
		tiles = new boolean[12];
	}
	
	public String toString()
	{
		String tilesRepresent = "";
		int j = 1;
		for (int i = 0; i < tiles.length; i++)
		{
			if (tiles[i] == false)
			{
				tilesRepresent = tilesRepresent + j + " ";
				j++;
			}
			else
			{
				tilesRepresent = tilesRepresent + "X" + " ";
				j++;
			}		
		}
		return tilesRepresent;
	}
	
	public boolean playATurn() 
	{
		die1.roll();
		die2.roll();
		System.out.println(die1.toString());
		System.out.println(die2.toString());
		
		int sumOfDice = die1.getFaceValue() + die2.getFaceValue();
		
		if (this.tiles[sumOfDice-1] == false)
		{
			this.tiles[sumOfDice-1] = true;
			System.out.println("Closing tile equal to sum: " + sumOfDice);
			return false;
		}
		else if (this.tiles[die1.getFaceValue()-1] == false)
		{
			this.tiles[die1.getFaceValue()-1] = true;
			System.out.println("Closing tile with the same value as die one: " + die1.getFaceValue());
			return false;
		}
		else if (this.tiles[die2.getFaceValue()-1] == false)
		{
			this.tiles[die2.getFaceValue()-1] = true;
			System.out.println("Closing tile with the same value as die two: " + die2.getFaceValue());			
			return false;
		}
		else 
		{
			System.out.println("All the tiles for these values are already shut");
			return true;
		}
	}
}