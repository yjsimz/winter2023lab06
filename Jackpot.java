public class Jackpot 
{
	public static void main(String[] args)
	{
		Board board = new Board();
		boolean gameOver = false;
		int numOfTilesClosed = 0;
	
		System.out.println("Welcome to Jackpot!");
		
		do
		{
			System.out.println(board);
			if (board.playATurn() == true)
			{
				gameOver = true;
			}
			else 
			{
				numOfTilesClosed++;
			}
		}
		while (gameOver == false);
		
		if (numOfTilesClosed >= 7)
		{
			System.out.println("You have reached the jackpot! Good Job!");
		}
		else
		{
			System.out.println("You lost :(");
		}
	}
}